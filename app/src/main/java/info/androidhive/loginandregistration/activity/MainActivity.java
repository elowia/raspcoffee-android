package info.androidhive.loginandregistration.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.HashMap;

import info.androidhive.loginandregistration.R;
import info.androidhive.loginandregistration.helper.SQLiteHandler;
import info.androidhive.loginandregistration.helper.SessionManager;
import android.os.Handler;
public class MainActivity extends Activity implements View.OnClickListener {

	private TextView txtName;
	private TextView txtEmail;
	private Button btnLogout;
	private Button bprogrammer;
	private Button beteindre;
	private Button ballumer;
	private Button bcheckHistory;
	private TextView tvstatut;
	private ProgressBar firstBar = null;
	private Handler handler = new Handler();
	boolean isStart;
	private int progressBarValue = 0;
	private TextView showText;

	private SQLiteHandler db;
	private SessionManager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		txtName = (TextView) findViewById(R.id.name);
		txtEmail = (TextView) findViewById(R.id.email);
		btnLogout = (Button) findViewById(R.id.btnLogout);
		bprogrammer = (Button) findViewById(R.id.bprogrammer);

		bprogrammer=(Button) findViewById(R.id.bprogrammer);
		bprogrammer.setOnClickListener(this);

		ballumer=(Button) findViewById(R.id.ballumer);
		ballumer.setOnClickListener(this);

		beteindre=(Button) findViewById(R.id.beteindre);
		beteindre.setOnClickListener(this);

		firstBar = (ProgressBar)findViewById(R.id.firstbar);
		showText = (TextView)findViewById(R.id.tvprogressbar);

		tvstatut=(TextView) findViewById(R.id.tvstatut);


		bcheckHistory=(Button) findViewById(R.id.bcheckHistory);
		bcheckHistory.setOnClickListener(this);

		// SqLite database handler
		db = new SQLiteHandler(getApplicationContext());

		// session manager
		session = new SessionManager(getApplicationContext());

		if (!session.isLoggedIn()) {
			logoutUser();
		}

		// Fetching user details from SQLite
		HashMap<String, String> user = db.getUserDetails();

		String name = user.get("name");
		String email = user.get("email");

		// Displaying the user details on the screen
		txtName.setText(name);
		txtEmail.setText(email);

		// Logout button click event
		btnLogout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				logoutUser();
			}
		});

		beteindre.setEnabled(false);
		firstBar.setVisibility(View.INVISIBLE);
		showText.setVisibility(View.INVISIBLE);



		handler = new Handler()
		{
			public void handleMessage(android.os.Message msg) {
				int toto=100;

					if (isStart) {
						if (progressBarValue != toto) {
							progressBarValue++;
							showText.setText("CAFE IN PROGRESS...");
						}
						else{
								showText.setText("CAFE IS READY ");
							}

					} else {
						progressBarValue = 0;
						showText.setVisibility(View.INVISIBLE);
						firstBar.setVisibility(View.INVISIBLE);
					}

					firstBar.setProgress(progressBarValue);
					handler.sendEmptyMessageDelayed(0, toto);


			}

		};

		handler.sendEmptyMessage(0);
	}




	/**
	 * Logging out the user. Will set isLoggedIn flag to false in shared
	 * preferences Clears the user data from sqlite users table
	 * */
	private void logoutUser() {
		session.setLogin(false);

		db.deleteUsers();

		// Launching the login activity
		Intent intent = new Intent(MainActivity.this, LoginActivity.class);
		startActivity(intent);
		finish();
	}



	@Override
	public void onClick(View view) {


		switch (view.getId()){

			case R.id.bprogrammer:
				startActivity(new Intent(this, Program.class));

				break;

			case R.id.ballumer:
				tvstatut.setText("Allumer");
				tvstatut.setTextColor(Color.parseColor("#82B239"));
				ballumer.setEnabled(false);
				beteindre.setEnabled(true);
				firstBar.setVisibility(View.VISIBLE);
				showText.setVisibility(View.VISIBLE);
				isStart = true;

				break;

			case R.id.beteindre:
				tvstatut.setText("Éteinte");
				tvstatut.setTextColor(Color.parseColor("#D81428"));
				ballumer.setEnabled(true);
				isStart = false;
				break;

			case R.id.bcheckHistory:
				startActivity(new Intent(this, History.class));
				break;
		}
	}
}
