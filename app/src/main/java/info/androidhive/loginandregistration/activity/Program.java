package info.androidhive.loginandregistration.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import info.androidhive.loginandregistration.R;
import info.androidhive.loginandregistration.activity.MainActivity;

public class Program extends AppCompatActivity  implements View.OnClickListener {

    private Button bsave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program);

        bsave =(Button) findViewById(R.id.bsave);
        bsave.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.bsave:
                startActivity( new Intent(this,MainActivity.class ));

                break;

        }
    }
}
